# fake-dl

Simulate a download.

## Usage

```
Usage: fake-download [OPTION]... [DOWNLOAD SIZE] [DOWNLOAD SPEED]
    -h, --help	display this help and exit
```

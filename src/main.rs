use core::{fmt, time};
use std::collections::HashMap;
use std::ops::RangeInclusive;
use std::process::{Command, Stdio};
use std::{env, thread};
use std::{io, io::Write};

use rand::{thread_rng, Rng};

#[allow(dead_code)]
enum ANSI {
    Reset,
    Bold,
    Dim,
    Italic,
    Underline,
    SlowBlink,
    FastBlink,
    Reverse,
    Hide,
    Strike,
    // Colors
    Black,
    Red,
    Green,
    Brown,
    Blue,
    Purple,
    Cyan,
    Grey,
}

impl ANSI {
    fn code(&self) -> u8 {
        match self {
            Self::Reset => 0,
            Self::Bold => 1,
            Self::Dim => 2,
            Self::Italic => 3,
            Self::Underline => 4,
            Self::SlowBlink => 5,
            Self::FastBlink => 6,
            Self::Reverse => 7,
            Self::Hide => 8,
            Self::Strike => 9,
            Self::Black => 30,
            Self::Red => 31,
            Self::Green => 32,
            Self::Brown => 33,
            Self::Blue => 34,
            Self::Purple => 35,
            Self::Cyan => 36,
            Self::Grey => 37,
        }
    }

    fn str(&self) -> String {
        format!("\x1b[{}m", self.code())
    }
}

impl fmt::Display for ANSI {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.str())
    }
}

fn u64_to_f64(n: u64) -> f64 {
    let n_f64 = n as f64;

    if n_f64 as u64 != n {
        panic!("Error: conversion from u64 to f64 failed");
    }

    n_f64
}

fn get_progress_bar(percentage: u8) -> String {
    let light_line = '\u{2500}'.to_string(); // ─
    let r_light_line = '\u{2576}'.to_string(); // ╶
    let heavy_line = '\u{2501}'.to_string(); // ━

    let total_steps: u8 = 40;

    let filled_steps: u8 = if percentage != 0 {
        ((total_steps as f32 / 100.) * percentage as f32) as u8
    } else {
        0
    };
    let remaining_steps: u8 = total_steps - filled_steps;

    if remaining_steps == 0 {
        format!(
            "{}{}{}",
            ANSI::Green,
            heavy_line.repeat(total_steps as usize),
            ANSI::Reset
        )
    } else {
        format!(
            "{}{}{}{}{}",
            ANSI::Red,
            heavy_line.repeat(filled_steps as usize),
            ANSI::Reset,
            r_light_line,
            light_line.repeat((remaining_steps - 1) as usize),
        )
    }
}

const fn get_byte_suffixes() -> [&'static str; 6] {
    ["B", "kB", "MB", "GB", "TB", "PB"]
}

fn get_human_readable_byte_size(byte_size: u64, show_unit: bool, separator: &str) -> String {
    let suffixes: [&str; 6] = get_byte_suffixes();
    let unit: f64 = 1000.;

    let byte_size_as_f64 = u64_to_f64(byte_size);

    if byte_size == 0 {
        return if show_unit {
            format!("0{}B", separator)
        } else {
            "0".to_string()
        };
    }

    let base = byte_size_as_f64.log10() / unit.log10();
    let result = unit.powf(base - base.floor());

    let hr_byte_size = format!("{:.1}", result).trim_end_matches(".0").to_owned();

    if show_unit {
        format!("{}{}{}", hr_byte_size, separator, suffixes[base as usize])
    } else {
        hr_byte_size
    }
}

fn get_eta(downloaded_size: u64, total_size: u64, download_speed: u64) -> String {
    let remaining_size = total_size - downloaded_size;
    let mut remaining_time = remaining_size / download_speed;

    let hours = if remaining_time >= 3600 {
        remaining_time / 3600
    } else {
        0
    };

    remaining_time -= hours * 3600;

    let minutes = if remaining_time >= 60 {
        remaining_time / 60
    } else {
        0
    };

    remaining_time -= minutes * 60;

    let seconds = remaining_time;

    format!("{}:{:0>2}:{:0>2}", hours, minutes, seconds,)
}

fn get_term_columns() -> u32 {
    let stty = Command::new("stty")
        .arg("size")
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("Error: failed to run `stty`");

    let output = stty
        .wait_with_output()
        .expect("Error: failed to wait on `stty`");

    let decoded_output = String::from_utf8_lossy(&output.stdout);

    let (_, columns) = {
        let split: Vec<&str> = decoded_output.trim_end().split(" ").collect();

        (
            split.get(0).unwrap().parse::<u32>().unwrap(),
            split.get(1).unwrap().parse::<u32>().unwrap(),
        )
    };

    columns
}

const fn usage() -> &'static str {
    "Usage: fake-download [OPTION]... [DOWNLOAD SIZE] [DOWNLOAD SPEED]
    -h, --help\tdisplay this help and exit"
}

fn get_byte_from_str(str: &str) -> Option<u64> {
    let suffixes = get_byte_suffixes();

    let mut unit_idx = 0;
    let mut value: String = str.to_string();

    // reverse the order so 'B' is tested in last and doesn't match others
    for i in (0..suffixes.len()).rev() {
        if str.ends_with(suffixes[i]) {
            unit_idx = i;
            value = str
                .get(0..(str.len() - suffixes[i].len()))
                .unwrap()
                .to_string();
            break;
        }
    }

    match value.parse::<u64>() {
        Ok(v) => Some(v * 1000_u64.pow(unit_idx as u32)),
        Err(_) => None,
    }
}

fn parse_args() -> HashMap<String, u64> {
    let args: Vec<String> = env::args().collect();
    let mut arg_count = 0;

    let mut parsed_args: HashMap<String, u64> = HashMap::new();

    for arg in args {
        if arg_count == 0 {
            arg_count += 1;
            continue;
        }

        if arg == "-h" || arg == "--help" {
            println!("{}", usage());
            std::process::exit(0);
        } else {
            let bytes = get_byte_from_str(&arg);

            if bytes.is_none() {
                panic!("Error: failed to parse the following argument `{}`", arg);
            }

            match arg_count {
                1 => parsed_args.insert("dl_size".to_string(), bytes.unwrap()),
                2 => parsed_args.insert("dl_speed".to_string(), bytes.unwrap()),
                _ => panic!("Error: too many arguments"),
            };
        }

        arg_count += 1;
    }

    parsed_args
}

fn main() {
    let args = parse_args();
    let mut rng = thread_rng();

    let dl_size: u64 = match args.get("dl_size") {
        Some(size) => size.to_owned(),
        None => rng.gen_range(10_000_000..=10_000_000_000), // [10MB 10GB]
    };

    let mut dl_speed: u64 = match args.get("dl_speed") {
        Some(speed) => speed.to_owned(),
        None => rng.gen_range(50_000_000..=200_000_000), // [50MB 200MB] in bytes per seconds
    };

    let update_per_s = 8;
    let speed_range_modifier: RangeInclusive<u64> = 10_000..=100_000; // [10kB 100kB]

    let mut downloaded_size: u64 = 0;

    let mut line: String;

    while downloaded_size < dl_size {
        downloaded_size += dl_speed / update_per_s;

        if downloaded_size > dl_size {
            downloaded_size = dl_size;
        }

        let speed_modifier = rng.gen_range(speed_range_modifier.clone());

        if rng.gen_bool(0.5) {
            dl_speed += speed_modifier;
        } else {
            if speed_modifier < dl_speed {
                dl_speed -= speed_modifier;
            }
        }

        let percentage = (u64_to_f64(downloaded_size) / u64_to_f64(dl_size) * 100.) as u8;
        let progress_bar = get_progress_bar(percentage);

        line = format!(
            "{}\r   {} {}{}/{}{} {}{}/s{} eta {}{}{}",
            " ".repeat(get_term_columns() as usize),
            progress_bar,
            ANSI::Green,
            get_human_readable_byte_size(downloaded_size, false, ""),
            get_human_readable_byte_size(dl_size, true, " "),
            ANSI::Reset,
            ANSI::Red,
            get_human_readable_byte_size(dl_speed, true, " "),
            ANSI::Reset,
            ANSI::Blue,
            get_eta(downloaded_size, dl_size, dl_speed),
            ANSI::Reset,
        );

        print!("{}", line);
        let _ = io::stdout().flush();

        thread::sleep(time::Duration::from_millis(1000 / update_per_s));

        if percentage != 100 {
            print!("\r");
        } else {
            print!("\n");
        }
    }
}
